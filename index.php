<?php

/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';
require 'lib/mysql.php';
require 'lib/httpcodes.php';
require 'lib/upload.php';
//require_once "lib/Mail-1.2.0/Mail.php";
require 'lib/send_mail.php';
require 'lib/randompass.php';
require_once 'swiftmailer/lib/swift_required.php';

\Slim\Slim::registerAutoloader();

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim();
//$app->config('debug', false);
$app->notFound(function () use ($app) {
    //$app->render('404.html');
            $code = "404";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
});


require("services/documentation.php");
require("services/photos.php");
require("services/user.php");
<<<<<<< HEAD
require("services/whilo.php");
require("services/points.php");
=======
>>>>>>> 6d39aa7ee7a0ae21ab4462612cb97d7668e45f5a
/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */


/*function handleError() {
   echo '<h1>ERROR!</h1>'; 
}
*/

$app->run();
