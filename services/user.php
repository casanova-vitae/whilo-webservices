<?php
// POST route User CRUD
// Create user when register
$app->post('/create_user',function () {
    if (//(isset($_POST['ci'])) && (!empty($_POST['ci'])) && 
        (isset($_POST['name'])) && (!empty($_POST['name'])) 
        && (isset($_POST['lastname'])) && (!empty($_POST['lastname'])) 
        && (isset($_POST['email'])) && (!empty($_POST['email'])) 
        /*&& (isset($_POST['age'])) && (!empty($_POST['age'])) 
        && (isset($_POST['gender'])) && (!empty($_POST['gender'])) 
        && (isset($_POST['address'])) && (!empty($_POST['address'])) 
        && (isset($_POST['country'])) && (!empty($_POST['country'])) 
        && (isset($_POST['city'])) && (!empty($_POST['city']))  
        && (isset($_POST['localphone'])) && (!empty($_POST['localphone']))  
        && (isset($_POST['phone'])) && (!empty($_POST['phone']))  */
        && (isset($_POST['password'])) && (!empty($_POST['password']))        
        ) {
            //$ci = trim($_POST['ci']);
            $name = trim($_POST['name']);
            $lastname = trim($_POST['lastname']);
            $email = trim($_POST['email']);
            /* $age  = trim($_POST['age']);
            $gender  = trim($_POST['gender']);
            $address  = trim($_POST['address']);
            $country  = trim($_POST['country']);
            $city  = trim($_POST['city']);
            $localphone  = trim($_POST['localphone']);
            $phone  = trim($_POST['phone']);*/
            $password = trim($_POST['password']);
            $password = sha1($password);


        }else{
            $code = "400";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
        }

        $db = connect_db();

        if (!file_exists('public_html/vendor/slim/slim/uploads/'.$email)) {
                        mkdir('public_html/vendor/slim/slim/uploads/'.$email, 0777, true);
        }else{

            $code = "611";
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();
        }
        
        if((isset($_POST['Base64Img'])) && (!empty($_POST['Base64Img']))){
                    
                    $Img = $_POST['Base64Img'];
                    $Base64Img = base64_decode($Img);
                    $file_name = $email.'.png';
                    $uploaded = file_put_contents('uploads/'.$email.'/'.$file_name, $Base64Img);
                    if($uploaded == false){
                        $code = "601";
                        $data = "";
                        $comment = httpcodes($code);
                        $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                        echo (json_encode($response));
                        exit();
                    }
                    
                }else{
                    $file_name = "";   
                }
       


        $result = $db->query( "INSERT INTO user ( user_name, user_lastname, user_email, user_password, user_points,user_active,user_picture) VALUES ( '$name', '$lastname', '$email','$password',30,1,'$file_name');");
        
        if($db->affected_rows == -1){
            $code = "602";
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();

        }
        if($db->affected_rows == 0){
            
            $code = "600";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();
            
        }
        if($db->affected_rows > 0){//Inserte El usuario Correctamente, ahora debo insertar las respuestas
                $last_id = $db->insert_id;// points operator 0 less 1 addition 
                $result = $db->query( "INSERT INTO points ( points_operator,points_add,user_id_user) VALUES (1,30,'$last_id');");

                if($db->affected_rows <= 0){
            
                    $code = "600";
                    $data = "Unable to add points to user:".$last_id." error:".$db->error;
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                    echo (json_encode($response));
                    $db->close();
                    exit();
                    
                }

                if($db->affected_rows > 0){
                    $code = "200";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                    echo (json_encode($response));
                    $db->close();
            }
        
    }
    }
);
// Get a specific user
$app->post('/read_user',function ()  {
        if ((isset($_POST['email'])) && (!empty($_POST['email']))) {
            $email = trim($_POST['email']);
        }else{
            $code = "400";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
        }
        $db = connect_db();
        $result = $db->query( "SELECT id_user FROM user WHERE user_email = '$email';" );
        $fila = mysqli_fetch_assoc($result);
        $id = $fila['id_user'];
              
        $result = $db->query( "SELECT u.id_user,u.user_name,u.user_lastname,u.user_email,u.user_points,u.user_active,u.user_picture,(SELECT COUNT(user_id_user_send) FROM media WHERE user_id_user_send = '$id') as pictures_send_count FROM user as u WHERE u.id_user = '$id' LIMIT 1;" );

        if($result == false){
            $code = "601";
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
        }

        while ( $row = $result->fetch_array(MYSQLI_ASSOC) ) {
            $data[] = $row;
        }
        $code = "200";
        $comment = httpcodes($code);
        $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
        echo (json_encode($response));
    $db->close();/* Close Connection*/
    }
);

// Get All Users of App
$app->post('/read_users',function ()  {
        
        $db = connect_db();
              
        $result = $db->query( 'SELECT id_user,user_name,user_lastname,user_email,user_points,user_active,user_picture FROM user;' );

        if($result == false){
            $code = "601";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
        }

        while ( $row = $result->fetch_array(MYSQLI_ASSOC) ) {
            //$response = array("step"=>"1","row"=>$row);
                    //echo (json_encode($response));
            $data[] = $row;
        }
        $code = "200";
        $comment = httpcodes($code);
        $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
        echo (json_encode($response));
    $db->close();/* Close Connection*/
    }
);

// Update fields  of a User
$app->post('/update_user',function () {
    $parameters ="";
    if ((isset($_POST['email'])) && (!empty($_POST['email'])) ) {
    $email = $_POST['email'];
    $name = $_POST['name'];
    $lastname = $_POST['lastname'];

    if((!empty($_POST['password']))){
        $password = trim($_POST['password']);
        $password = sha1($password);
    }else{
        $password = '';
    }
    

    

    if((isset($_POST['Base64Img'])) && (!empty($_POST['Base64Img']))){
                    $parameters .= " user_picture = '".($_POST['Base64Img'])."'";
                    $Img = $_POST['Base64Img'];
                    $Base64Img = base64_decode($Img);
                    $file_name = $email.'.png';
                    $uploaded = file_put_contents('uploads/'.$email.'/'.$file_name, $Base64Img);
                    if($uploaded == false){
                        $code = "601";
                        $data = "";
                        $comment = httpcodes($code);
                        $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                        echo (json_encode($response));
                        exit();
                    }
                    
                }else{
                    $file_name ='';
                }

    $db = connect_db();
    $result = $db->query( "SELECT id_user FROM user WHERE user_email = '$email';" );
    $fila = mysqli_fetch_assoc($result);
    $id = $fila['id_user'];

    
    //$sql = "UPDATE user SET  ".$parameters." WHERE id_user = '$id'";
    $sql = "UPDATE user SET  user_name=IF(LENGTH('$name')=0,user_name,'$name'),user_lastname=IF(LENGTH('$lastname')=0,user_lastname, '$lastname'), user_password=IF(LENGTH('$password')=0,user_password, '$password'), user_picture=IF(LENGTH('$file_name')=0,user_picture, '$file_name') WHERE id_user = '$id';";
    $result = $db->query( $sql);
    
    /*if($db->affected_rows == -1){
            $code = "400";
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("in"=>$id,"code"=>$code, "response"=>$data, "comment"=>$comment);
            //echo (json_encode($response));
            $db->close();
            exit();

        }*/
            if($db->affected_rows <= 0){
                $code = "600";
                $data = $db->error;
                $comment = httpcodes($code);
                $response = array("code"=>$code, "response"=>$data."Z".$parameters, "comment"=>$comment);
                echo (json_encode($response));
                $db->close();
                exit();
                
            }
            if($db->affected_rows > 0){
                $code = "200";
                $data = "";
                $comment = httpcodes($code);
                $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                echo (json_encode($response));
                $db->close();
                
            }

        }else{
            $code = "400";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
    }
}
);

// Delete an specific user (Does not delet users, only change active status)
$app->post('/delete_user',function () {
    $parameters ="";
    $email = trim($_POST['email']);
    $db = connect_db();
    $result = $db->query( "SELECT id_user FROM user WHERE user_email = '$email';" );
    $fila = mysqli_fetch_assoc($result);
    $id = $fila['id_user'];

    $sql = "UPDATE user SET user_active = 0  WHERE id_user = '$id'";
    $result = $db->query( $sql);
    if($db->affected_rows == -1){
            $code = $parameters;
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();

        }
        if($db->affected_rows == 0){
            $code = "600";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();
            
        }
        if($db->affected_rows > 0){
            $code = "200";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            
        }

    }
);

// Delete an specific user (Does not delet users, only change active status)
$app->post('/undelete_user',function () {
    $parameters ="";
    $email = trim($_POST['email']);
    $db = connect_db();
    $result = $db->query( "SELECT id_user FROM user WHERE user_email = '$email';" );
    $fila = mysqli_fetch_assoc($result);
    $id = $fila['id_user'];

    $sql = "UPDATE user SET user_active = 1  WHERE id_user = '$id'";
    $result = $db->query( $sql);
    if($db->affected_rows == -1){
            $code = $parameters;
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();

        }
        if($db->affected_rows == 0){
            $code = "600";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            exit();
            
        }
        if($db->affected_rows > 0){
            $code = "200";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            $db->close();
            
        }

    }
);

// Login User
$app->post('/login_user',function () {

    if ((isset($_POST['email'])) && (!empty($_POST['email']))  && (isset($_POST['password'])) && (!empty($_POST['password']))  ) {
            $password = sha1( $_POST['password']);
            $email = $_POST['email'];
            $db = connect_db();

            $result = $db->query( "SELECT id_user FROM user WHERE user_email = '$email';" );
            $fila = mysqli_fetch_assoc($result);
            $id = $fila['id_user'];

            //if ( $result = $db->query( "SELECT * FROM user WHERE user_email='$email' and user_password ='$password' LIMIT 1;" )) {
            if ( $result = $db->query( "SELECT id_user,user_name,user_lastname,user_email,user_picture,user_points,user_timestamp,(SELECT COUNT(m.user_id_user_send) FROM  media as m WHERE m.user_id_user_send = '$id'  ) as count FROM user WHERE user_email='$email' and user_password ='$password' LIMIT 1;" )) {
                if ($db->affected_rows == 0){
                    
                    //$code = "200";
                    $code = "606";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                    echo (json_encode($response));

                }else{
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    $code = "200";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$row, "comment"=>$comment);
                    echo (json_encode($response));
                }
            }else{
                    $code = "601";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                    echo (json_encode($response));

            }

            $db->close();
      }else{
            $code = "400";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();

      }  
    }
);

$app->post('/question_user',function () {
                $array_q = array("recovery_q01","recovery_q02","recovery_q03","recovery_q04");
                shuffle($array_q);
                $db = connect_db();
                $q1 = $array_q[0];
                $q2 = $array_q[1];
                $result = $db->query( "SELECT * FROM recovery;" );

                        if($result == false){
                        $code = "601";
                        $data = "";
                        $comment = httpcodes($code);
                        $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                        echo (json_encode($response));
                        exit();
                        }else{
                            $data = $result->fetch_array(MYSQLI_ASSOC);
                            $code = "200";
                            //$data =   $result->fetch_array(MYSQLI_ASSOC);
                            //$data = mysqli_fetch_array($result, MYSQLI_ASSOC);
                            $comment = httpcodes($code);
                            
                            $response = array("code"=>$code, "response"=>$data,"token_code_01"=>$array_q[0],"token_code_02"=>$array_q[1], "comment"=>$comment);
                            echo (json_encode($response));

                        }

                }
    
);

$app->post('/recovery_user',function () {

    if ((isset($_POST['email'])) && (!empty($_POST['email'])) ) {
         
            $email = trim($_POST['email']);
            $db = connect_db();

            if ( $result = $db->query( "SELECT * FROM user WHERE user_email='$email'  LIMIT 1;" )) {

                if ($db->affected_rows == 0){
                    
                    //$code = "200";
                    $code = "606";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                    echo (json_encode($response));

                }else{
                    /*$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    $code = "200";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$row, "comment"=>$comment);
                    echo (json_encode($response));*/
                        $result = $db->query( "SELECT id_user FROM user WHERE user_email = '$email';" );
                        $fila = mysqli_fetch_assoc($result);
                        $id = $fila['id_user'];
                        $auto_pass = randomPassword();
                        $auto_pass_sha1 = sha1($auto_pass);
                        $sql = "UPDATE user SET  user_password='$auto_pass_sha1' WHERE id_user = '$id'";
                        $result = $db->query( $sql);
                        if($db->affected_rows == -1){
                                $code = $parameters;
                                $data = $db->error;
                                $comment = httpcodes($code);
                                $response = array("in"=>$id,"code"=>$code, "response"=>$data, "comment"=>$comment);
                                echo (json_encode($response));
                                $db->close();
                                exit();

                            }
                            if($db->affected_rows == 0){
                                $code = "600";
                                $data = "";
                                $comment = httpcodes($code);
                                $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
                                echo (json_encode($response));
                                $db->close();
                                exit();
                                
                            }
                            if($db->affected_rows > 0){
                                $data = send_mail($email,$auto_pass);
                               // $data = "";
                                $code = "200";
                                $comment = httpcodes($code);
                                $response = array("code"=>$code, "response"=>$auto_pass, "comment"=>$comment);
                                echo (json_encode($response));
                                $db->close();
                                
                            }
                }
            }else{
                    $code = "601";
                    $data = "";
                    $comment = httpcodes($code);
                    $response = array("code"=>$code, "response"=>$row, "comment"=>$comment);
                    echo (json_encode($response));

            }

            //$db->close();
      }  
    }
);


// Get a specific user
$app->post('/read_notify',function ()  {
        if ((isset($_POST['email'])) && (!empty($_POST['email']))) {
            $email = trim($_POST['email']);
        }else{
            $code = "400";
            $data = "";
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
        }
        $db = connect_db();
        $result = $db->query( "SELECT * FROM user WHERE user_email = '$email';" );
        $fila = mysqli_fetch_assoc($result);
        $id = $fila['id_user'];
              
        $result = $db->query( "SELECT 'send_photo' as comand,user_id_user_send as email,user_id_user_arrival as receptor, media_timestamp as user_timestamp FROM media 
            WHERE user_id_user_send = '$id'
            UNION ALL 
            SELECT 'recive_photo' as comand,user_id_user_arrival,user_id_user_send, media_timestamp as user_timestamp 
            FROM media 
            WHERE user_id_user_arrival = '$id'
            UNION ALL 
            SELECT 'request_whilo' as comand,user_id_user,user_id_user_add, user_has_user_timestamp as user_timestamp 
            FROM user_has_user 
            WHERE user_id_user = '$id' AND user_has_user_type = 'request'
            UNION ALL 
            SELECT 'add_whilo' as comand,user_id_user,user_id_user_add, user_has_user_timestamp as user_timestamp 
            FROM user_has_user 
            WHERE user_id_user = '$id' AND user_has_user_type = 'whilo'
            order by user_timestamp ;" );

        if($result == false){
            $code = "601";
            $data = $db->error;
            $comment = httpcodes($code);
            $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
            echo (json_encode($response));
            exit();
        }

        while ( $row = $result->fetch_array(MYSQLI_ASSOC) ) {
            $data[] = $row;
        }
        $code = "200";
        $comment = httpcodes($code);
        $response = array("code"=>$code, "response"=>$data, "comment"=>$comment);
        echo (json_encode($response));
    $db->close();/* Close Connection*/
    }
);
?>